#!/bin/bash -e

if [ $# -eq 0 ]
  then
    echo "Usage: $0 account_name"
    exit 1
fi
account_name=$1

function docker_run {
    docker run --rm \
        --user $(id -u) \
        -v "$(pwd):/app:ro" \
        -v "/tmp:/tmp" \
        $@
}

cd "$(dirname "$0")"
echo "# build docker images if not existing yet"
docker build -t kh-dump-transactions ./dump_transactions
docker build -t kh-convert-transactions ./convert_transactions

echo ""
echo "# dump transactions"
docker_run \
    kh-dump-transactions \
    python /app/dump_transactions/dump_transactions.py $account_name /tmp/transactions.json

echo ""
echo "# convert transactions"
docker_run \
    --entrypoint /node_modules/.bin/ts-node \
    kh-convert-transactions \
    /app/convert_transactions/convert.ts /tmp/transactions.json $account_name /tmp/$account_name.csv

echo ""
echo "# open libreoffice"
libreoffice --infilter=CSV:44,34,0,1,1/5,UTF8 /tmp/$account_name.csv &

sleep 5



