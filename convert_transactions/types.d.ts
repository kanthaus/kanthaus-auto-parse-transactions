export type KostenErlösArt =
    'Arbeitsmittel' |
    'Beiträge, Gebühren, Abgaben und Versicherungen' |
    'Entsorgung' |
    'GWG' |
    'Kostenneutral' |
    'Nicht abzugsfähige Ausgaben' |
    'Raumkosten' |
    'Sonstige Fahrtkosten' |
    'KFZ-Steuer & Versicherung, Maut' |
    'Übrige Betriebsausgaben' |
    'Übrige Schuldzinsen' |
    'umsatzsteuerfrei' |
    'Umsatzsteuerpflichtige Betriebseinnahmen' |
    'AFA bewegliche Wirtschaftsgüter' |
    'AFA unbewegliche Wirtschaftsgüter'|
    'Umsatzsteuerfreie Betriebseinnahmen'

export type KanthausKategorie = 
    // Incoming
    'donation' | 
    'pv_income' | 
    'funding' | 
    'rent_person' | 
    'rent_external' | 
    'other_income' | 

    // Outgoing
    'administration' | 
    'bikes' | 
    'building_administration' | 
    'building_rouble' | 
    'car_adminstration' | 
    'car_fuel' | 
    'car_repairs_material' | 
    'construction' | 
    'food' | 
    'general_other' | 
    'household_consumables' | 
    'utilities' | 
    'insurance' | 
    'internet_phone_it' | 
    'workshop' | 

    // Special 
    're-donatable' | 
    'internal' | 
    'loans' | 
    'external' 


export interface BuchhaltungsTransaction {
    ZahlDatum: string
    EntryId: string
    Konto: 'Bank',
    BelegNr: string|null,
    EinnahmeAusgabe: 'Einnahme'|'Ausgabe'|''
    Bruttobetrag: string
    ignored?: boolean
    KostenErlösArt: KostenErlösArt | '' | '???'
    Buchungstext: string
    Zusatztext: string
    Projekte: string
    KanthausKategorie: KanthausKategorie | ''
}

export interface FinTSTransaction {
    // "status": "D",
    // "funds_code": "R",
    amount: {
        amount: string
        //   "currency": "EUR"
    }
    // "id": "NDDT",
    // "customer_reference": null,
    // "bank_reference": null,
    // "extra_details": "",
    // "currency": "EUR",
    date: string
    entry_date: string|undefined
    // "guessed_entry_date": "2021-12-08",
    // "transaction_code": "105",
    posting_text: 'FOLGELASTSCHRIFT' | 'KARTENZAHLUNG' | 'GUTSCHR. UEBERWEISUNG' | 'GUTSCHR. UEBERW. DAUERAUFTR' | 'ENTGELTABSCHLUSS' | 'ONLINE-UEBERWEISUNG' | 'BARGELDAUSZAHLUNG',
    // "prima_nota": "9250",
    purpose: string
    // "applicant_bin": "DEUTDEFF",
    applicant_iban: string
    applicant_name: string
    // "return_debit_notes": "992",
    // "recipient_name": null,
    "additional_purpose": null,
    // "gvc_applicant_iban": null,
    // "gvc_applicant_bin": null,
    end_to_end_reference: string
    additional_position_reference: string,
    // "applicant_creditor_id": "LU96ZZZ0000000000000000058",
    // "purpose_code": null,
    // "additional_position_date": null,
    // "deviate_applicant": null,
    // "deviate_recipient": null,
    // "FRST_ONE_OFF_RECC": null,
    // "old_SEPA_CI": null,
    // "old_SEPA_additional_position_reference": null,
    // "settlement_tag": null,
    // "debitor_identifier": null,
    // "compensation_amount": null,
    // "original_amount": null

    fullPurpose: string // aggreated
}