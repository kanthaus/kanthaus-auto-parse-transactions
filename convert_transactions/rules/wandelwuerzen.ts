import type { convertRule, convertReturn } from '../lib/convertBankToBuchungsFormat'

import { BuchhaltungsTransaction, FinTSTransaction } from "../types"

const rules: convertRule[] = [
    {
        purposeContains: 'spende',
        cb({ applicant_name }): convertReturn {
            return {
                Buchungstext: applicant_name,
                Zusatztext: 'Spende',
                // TODO: KostenErlösArt: 
                KanthausKategorie: 'donation',
              }
        }
    },
    
    {
        applicantNameIs: 'Haus Kante Wurzen w.V.',
        purposeContains: 'Miete',
        cb({ applicant_name }): convertReturn {
            return {
                Buchungstext: applicant_name,
                Zusatztext: 'Miete Kantstr. 22 EG',
              }
        }
    },

    {
        applicantNameIs: 'Telekom Deutschland GmbH',
        cb({ fullPurpose }): convertReturn {
            const rgId = fullPurpose.match(/ RG (\d+)\//)?.[1]
            if (rgId) {
                return {
                    Buchungstext: 'Telekom Deutschland GmbH',
                    Zusatztext: `Rechnung RG ${rgId}`,
                    // ToDo: KostenErlösArt: 
                    KanthausKategorie: 'internet_phone_it'
                }
            }
        }
    },

    {
        applicantNameIs: 'Verbund Offener Werkstätten (e.V.)',
        purposeContains: 'haftpflichtversicherung',
        cb({ applicant_name }): convertReturn {
          return {
            Buchungstext: applicant_name,
            Zusatztext: `Haftpflichtversicherung VOW e.V.`,
          }
        }
    },


    // filter
    {
        applicantNameContains: 'PayPal',
        cb({ applicant_name, fullPurpose, end_to_end_reference, additional_position_reference }): convertReturn {
            const buchungstext = fullPurpose.match(/^.*?\. (.*?),/)?.[1]
            if(buchungstext)  {
                return  {
                    Buchungstext: buchungstext,
                    Zusatztext: end_to_end_reference + ' ' + additional_position_reference
                }
            }
        }
    },


]

export default rules