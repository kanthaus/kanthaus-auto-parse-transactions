import type { convertRule, convertReturn } from '../lib/convertBankToBuchungsFormat'

import { BuchhaltungsTransaction } from "../types"

function rentK20(name: string, isInternal = false): Partial<BuchhaltungsTransaction> {
    return  {
        Buchungstext: name,
        Zusatztext: 'Miete Kantstr. 20',
        KostenErlösArt: 'Umsatzsteuerfreie Betriebseinnahmen',
        KanthausKategorie: 'rent_person'
    }
}

const rules: convertRule[] = [

    // Utilities
    {
        applicantNameIs: 'Abwasserzweckverband Muldenaue',
        cb({ fullPurpose }): convertReturn {
            let Zusatztext = ''
            if (fullPurpose.includes('Abwasser')) {
                Zusatztext = 'Abschlag Abwasser K20'
            } else if (fullPurpose.includes('Niederschl')) {
                Zusatztext = 'Niederschlagsabwasser'
            }
            return {
                Buchungstext: 'Abwasserzweckverband Muldenaue',
                Zusatztext,
                KostenErlösArt: 'Raumkosten', // TODO
                KanthausKategorie: 'utilities'
            }
        }
    },
    {
        applicantNameIs: 'VERSORGUNGSVERBAND',
        cb({ fullPurpose }): convertReturn {
            let Zusatztext: string = ''
            const strasseNum = fullPurpose.match(/KANTSTR\. (\d{2})$/)?.[1]
            if (fullPurpose.includes('ABSCHLAG')) {
                Zusatztext = `Abschlag Wasser Kantstr. ${strasseNum}`
            }
            else if (fullPurpose.includes('VORAUSZAHLUNG')) {
                Zusatztext = `Vorauszahlung Wasser Kantstr. ${strasseNum}`
            }
            
            else {
                Zusatztext = fullPurpose
            }
            return {
                Buchungstext: 'Versorgungsverband Eilenburg Wurzen',
                Zusatztext: Zusatztext,
                KostenErlösArt: 'Raumkosten',
                KanthausKategorie: 'utilities',
            }
        }
    },
    {
        applicantNameIs: 'Elektrizitätswerke SchönauVertriebs GmbH',
        cb({ fullPurpose}): convertReturn {
            const p = fullPurpose.split('/').slice(1,3).reverse().map(a => a.trim())
            return {
                Buchungstext: 'Elektrizitätswerke SchönauVertriebs GmbH',
                Zusatztext: p.join(', '),
                KostenErlösArt: 'Raumkosten',
                KanthausKategorie: 'utilities',
            }
        }
    },
 
    {
        applicantNameIs: 'Landkreis Leipzig',
        purposeContains: 'E10735490',
        cb({ fullPurpose }): convertReturn {
            
            return {
                Zusatztext: 'Abfallgebührenbescheid Verein',
                KostenErlösArt: 'Raumkosten', 
                KanthausKategorie: 'utilities'
            }
        }
    },
    {
        applicantNameIs: 'Landkreis Leipzig',
        purposeContains: 'E10105480',
        cb({ fullPurpose }): convertReturn {
            
            return {
                Zusatztext: 'Abfallgebührenbescheid Mitglieder',
                KostenErlösArt: 'Raumkosten', 
                KanthausKategorie: 'utilities'
            }
        }
    },

    //PV_income
    {
        applicantNameIs: 'Mitteldeutsche Netzgesellschaft Strom mbH',
        cb({ fullPurpose}): convertReturn {
            const p = fullPurpose.split('/').slice(1,3).reverse().map(a => a.trim())
            return {
                Buchungstext: 'Mitteldeutsche Netzgesellschaft Strom mbH',
                Zusatztext: 'PV-Ertrag Kantstr. 20',
                KostenErlösArt: 'Umsatzsteuerpflichtige Betriebseinnahmen', 
                KanthausKategorie: 'pv_income'
            }
        }
    },


    // Rents
    {
        applicantNameContains: 'DELPEUCH',
        purposeContains: 'Miete',
        cb(): convertReturn {
            return rentK20('Antonin Delpeuch')
        }
    },
    {
        applicantNameIs: 'Matthias Larisch',
        purposeContains: 'Miete',
        cb(): convertReturn {
            return rentK20('Matthias Larisch')
        }
    },
    {
        applicantNameIs: 'Wandel würzen e.V.',
        purposeContains: 'Miete',
        cb(): convertReturn {
            return {
                Zusatztext: 'Miete Kantstr. 22',
                KostenErlösArt: 'Umsatzsteuerfreie Betriebseinnahmen',
                KanthausKategorie: 'internal'
            }
        }
    },
    {
        applicantNameContains: 'Thore Peyk',
        purposeContains: 'Miete',
        cb(tx): convertReturn {
            // console.log(tx.applicant_name)
            return rentK20('Thore Peyk')
        }
    },
    {
        applicantNameIs: 'Landkreis Leipzig',
        purposeContains: 'Miete Neiser',
        cb(): convertReturn {
            return rentK20('Landkreis Leipzig / David Neiser')
        }
    },
    {
        applicantNameContains: 'LATHUILIERE MAXIME',
        purposeContains: 'Miete',
        cb(): convertReturn {
            return rentK20('Maxime Lathuilière')
        }
    },
    {
        applicantNameContains: 'Janina',
        purposeContains: 'Miete Becker',
        cb(): convertReturn {
            return rentK20('Janina Becker', true)
        }
    },
    {
        applicantNameIs: 'Felix Wenzel',
        purposeContains: 'Miete',
        cb(): convertReturn {
            return rentK20('Felix Wenzel')
        }
    },
    {
        applicantNameIs: 'Douglas Miles Hugh Webb',
        purposeContains: 'Miete',
        cb(): convertReturn {
            return rentK20('Douglas Webb')
        }
    },
    {
        applicantNameIs: 'Larissa Brünjes',
        purposeContains: 'Miete',
        cb(): convertReturn {
            return rentK20('Larissa Brünjes')
        }
    },
    {
        applicantNameIs: 'Katharina Seibt',
        purposeContains: 'Miete',
        cb(): convertReturn {
            return rentK20('Katharina Seibt')
        }
    },


    // administration
    {
        purposeContains: 'Kontoführungspreis',
        cb(): convertReturn {
            return {
                Buchungstext: 'Sparkasse Muldental',
                Zusatztext: 'Kontoführungsgebühren',
                KostenErlösArt: 'Übrige Betriebsausgaben',
                KanthausKategorie: 'administration'
            }
        }
    },
    {
        applicantNameIs: 'Grosse Kreisstadt Wurzen',
        cb({ fullPurpose }): convertReturn {
            let Zusatztext
            if (fullPurpose.includes('GRUND')) {
                const strasseNum = fullPurpose.match(/KANTSTR\. (\d{2})$/)?.[1]
                Zusatztext = `Grundsteuer Kantstr. ${strasseNum}`
            } else if (fullPurpose.includes('001-STRASSENREINIGUN')) {
                Zusatztext = 'Straßenreinigung K-20'
            } else if (fullPurpose.includes('002-STRASSENREINIGUN')) {
                Zusatztext = 'Straßenreinigung K-22'
            } else {
                Zusatztext = fullPurpose
            }
            return {
                Buchungstext: 'Große Kreisstadt Wurzen',
                Zusatztext,
                KostenErlösArt: 'Raumkosten',
                KanthausKategorie: 'building_administration'
            }
        }
    },

    {
        applicantNameIs: 'Bundeskasse in Halle',
        purposeContains: 'Kfz-Steuer',
        cb({ fullPurpose }): convertReturn {
            let Zusatztext = ''
            if (fullPurpose.startsWith('Kfz-Steuer fuer WUR A667')) {
                Zusatztext = 'Kfz-Steuer KeinMüllWagen (WUR A-667)'
            } else if (fullPurpose.startsWith('Kfz-Steuer fuer WUR A707')) {
                Zusatztext = 'Kfz-Steuer Anhänger (WUR A-707)'
            }
            return {
                Buchungstext: 'Bundeskasse in Halle',
                Zusatztext,
                KostenErlösArt: 'KFZ-Steuer & Versicherung, Maut',
                KanthausKategorie: 'car_adminstration',
            }
        }
    },
    {
        applicantNameIs: 'HDI GlobalSE',
        cb(): convertReturn {
            return {
                Buchungstext: 'HDI GlobalSE',
                Zusatztext: 'Kfz-Versicherung KeinMüllWagen',
                KostenErlösArt: 'KFZ-Steuer & Versicherung, Maut',
                KanthausKategorie: 'car_adminstration',
            }
        }
    },
    
    {
        applicantNameIs: 'NÜRNBERGER Allgemeine Versicherungs-Aktiengesellschaft',
        cb({ applicant_name }): convertReturn {
            return {
                Buchungstext: applicant_name,
                Zusatztext: 'Haftpflichtversicherung Verein',
                KostenErlösArt: 'Beiträge, Gebühren, Abgaben und Versicherungen',
                KanthausKategorie: 'insurance'
            }
        }
    },

    // other
    {
        condition: ({ posting_text }) => posting_text === 'BARGELDAUSZAHLUNG',
        cb(): convertReturn {
            return {
                Buchungstext: 'Bargeldauszahlung',
                EinnahmeAusgabe: '',
                Zusatztext: 'Transfer in Kasse',
                KostenErlösArt: 'Kostenneutral',
                KanthausKategorie: 'internal'
            }
        }
    },
    {
        applicantNameIs: 'GAVEG',
        cb({ fullPurpose }): convertReturn {
            return {
                Buchungstext: 'GAVEG',
                Zusatztext: 'Autogas ' + fullPurpose.split(' ')[0],
                KostenErlösArt: 'Sonstige Fahrtkosten',
                KanthausKategorie: 'car_fuel'
            }
        }
    },


    // filter
    {
        applicantNameContains: 'PayPal',
        cb({ applicant_name, fullPurpose, end_to_end_reference, additional_position_reference }): convertReturn {
            const buchungstext = fullPurpose.match(/^.*?\. (.*?),/)?.[1]
            if(buchungstext)  {
                return  {
                    Buchungstext: buchungstext,
                    Zusatztext: end_to_end_reference + ' ' + additional_position_reference
                }
            }
        }
    },
    {
        condition: ({amount, fullPurpose, posting_text}) => !parseFloat(amount.amount) && (fullPurpose.match(/Abrechnung/) || posting_text === 'ENTGELTABSCHLUSS'),
        cb() {
            return {
                ignored: true
            }
        }
    }


]

export default rules