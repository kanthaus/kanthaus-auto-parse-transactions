import * as fs from 'fs'
import { BuchhaltungsTransaction, FinTSTransaction } from './types'
import { unparse as json2csv } from 'papaparse'
import convertBankToBuchungsFormat from './lib/convertBankToBuchungsFormat'
import rulesHKW from './rules/hkw'
import rulesWaWue from './rules/wandelwuerzen'


function print_usage() {
    console.log('USAGE: npm run convert input_filename [account_name] [output_filename]')
}

/* =================== */
/* Parameter parsing   */
/* =================== */
const input_file = process.argv[2]
if (!input_file) {
    console.error('no input file provided')
    print_usage()
    process.exit(1)
}

const output_file = process.argv[4]
let accountName = process.argv[3]
if (!accountName) {
    if (input_file.includes('hkw')) {
        accountName = 'hkw'
    } else if (input_file.includes('wandelwuerzen')) {
        accountName = 'wandelwuerzen'
    }
}

if (!accountName) {
    console.error('file name should contain either "hkw" or "wandelwuerzen"')
    print_usage()
    process.exit(1)
}

const rules = accountName == 'hkw' ? rulesHKW : rulesWaWue;

/* =================== */
/* data processing     */
/* =================== */
const data: FinTSTransaction[] = JSON.parse(fs.readFileSync(input_file, 'utf-8'))
// console.log(data)

const out: BuchhaltungsTransaction[] = []

for(let tx of data) {
    const res = convertBankToBuchungsFormat(tx, rules)
    if(!res || res.ignored) continue
    out.push(res)
}



/* =================== */
/* output              */
/* =================== */
const buchhaltungColumns = [
    'ZahlDatum',
    'EntryId',
    'Konto',
    'BelegNr',
    'Buchungstext',
    'Zusatztext',
    'EinnahmeAusgabe',
    'KostenErlösArt',
    'Bruttobetrag',
    'Vereinsbereich',
    'Projekt',
    'KanthausKategorie'
]

const csv = json2csv({
    fields: buchhaltungColumns,
    data: out,
})

if (process.argv[4]) {
    fs.writeFileSync(process.argv[4], csv, 'utf-8')
    console.log(`written to ${process.argv[4]}`)
} else {
    console.log(csv)
}

