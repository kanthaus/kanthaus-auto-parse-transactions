import { FinTSTransaction } from "../types"

export function cleanupTextField(text: string): string {
    text = text || ''
    return text
        .normalize()
        .trim()
        .replace(/^[.-]+/, '')
        .trim()
}
  
export function getDefaultText(lineData: FinTSTransaction, fields: string[], includeFieldLabel: boolean ) {
    let zusatztext = ''
    for (const field of fields) {
      const value = (lineData as any)[field]
      const fieldTxt = includeFieldLabel ? `${field}=` : ''
      if (value) zusatztext += `${fieldTxt}${cleanupTextField(value)} | `
    }
    return zusatztext.trim().replace(/\|$/, '').trim()
}

export function formatDate(date: string): string {
    if(date && date.match(/^\d{4}-\d{2}-\d{2}/)) {
        return date.slice(0,11)
    } else if (date) {
        return new Date(date).toISOString().split('T')[0]
    } else {
        return ''
    }
}
  


const oneDay = 24 * 60 * 60 * 1000
 
function getDayOfYear(day: string) {
    const year = new Date(day).getFullYear()
    const msSinceYearStarted = Date.parse(day) - Date.parse(year.toString())
    return Math.floor(msSinceYearStarted / oneDay) + 1
}

const entryIdCounters: {[base: string]: number} = {}

export function getEntryId(date: string): string {
  const EntryIdBase = getDayOfYear(date).toString().padStart(3, '0')
  entryIdCounters[EntryIdBase] = entryIdCounters[EntryIdBase] || 0
  const entryIdNum = ++entryIdCounters[EntryIdBase]
  return `${EntryIdBase}.${entryIdNum}`
}
