import { BuchhaltungsTransaction, FinTSTransaction } from "../types";
import { formatDate, getDefaultText, getEntryId } from "./utils";

export type convertReturn = Partial<BuchhaltungsTransaction>|void
export interface convertRule {
    applicantNameIs?: string
    applicantNameContains?: string
    purposeContains?: string
    purposeMatches?: RegExp
    condition?: (tx: FinTSTransaction) => any
    cb: (input: FinTSTransaction) => convertReturn
}


const defaultBuchungstextFields = [
    'applicant_name',
    // 'purpose',
]

const defaultZusatztextFields = [
    // 'applicant_bin',
    // 'applicant_iban',
    // 'applicant_name',
    // 'applicant_creditor_id',
    // 'purpose',
    // 'additional_purpose',
    'purpose_code',
    // 'return_debit_notes',
    'recipient_name',
    'gvc_applicant_iban',
    'gvc_applicant_bin',
    'end_to_end_reference',
    'additional_position_reference',
    'additional_position_date',
    'deviate_applicant',
    'deviate_recipient',
    'FRST_ONE_OFF_RECC',
    'old_SEPA_CI',
    'old_SEPA_additional_position_reference',
    // 'posting_text',
    'settlement_tag',
    'debitor_identifier',
    'compensation_amount',
    'original_amount',
]


export default function convertBankToBuchungsFormat(input: FinTSTransaction, rules: convertRule[]): BuchhaltungsTransaction|null {
    const date = input.entry_date || input.date
    const amount = parseFloat(input.amount.amount)


    input.fullPurpose = ((input.purpose || '') +' ' + (input.additional_purpose || '')).trim()

    const defaultZusatzText = getDefaultText(
        input,
        defaultZusatztextFields,
        true,
    ).trim()
    const output: BuchhaltungsTransaction = {
        ZahlDatum: formatDate(date),
        EntryId: getEntryId(date),
        Konto: 'Bank',
        EinnahmeAusgabe: amount ? (amount > 0 ? 'Einnahme' : 'Ausgabe') : '',
        Bruttobetrag: amount.toLocaleString('de').replace(/\./g, ''),
        BelegNr: '',
        KostenErlösArt: '',
        Buchungstext: getDefaultText(
            input,
            defaultBuchungstextFields,
            false,
        ),
        Zusatztext: input.fullPurpose + (defaultZusatzText ? ' | '+defaultZusatzText : ''),
        Projekte: '',
        KanthausKategorie: ''
    }

    for(let rule of rules) {

        if(rule.condition) {
            if(!rule.condition(input)) continue
        }
        if(rule.applicantNameIs || rule.applicantNameIs === null) {
            if(!input.applicant_name || rule.applicantNameIs.toLocaleLowerCase() !== input.applicant_name.toLocaleLowerCase()) continue
        }
        if(rule.applicantNameContains || rule.applicantNameContains === null) {
            if(!input.applicant_name || !input.applicant_name.toLocaleLowerCase().includes(rule.applicantNameContains.toLocaleLowerCase())) continue
        }
        if(rule.purposeContains || rule.purposeContains === null) {
            if(!input.fullPurpose.toLocaleLowerCase().includes(rule.purposeContains.toLocaleLowerCase())) continue
        }
        if(rule.purposeMatches || rule.purposeMatches === null) {
            if(!input.fullPurpose.match(rule.purposeMatches)) continue
        }

        // all conditions match
        const res = rule.cb(input)    
        if(res) {
            Object.assign(output, res)
            return output
            return null
        }
    }    

    // no rule applied
    console.log('TODO = '+getDefaultText(
        input,
        [...defaultBuchungstextFields, ...defaultZusatztextFields],
        true,
    ))
    console.log(input)
    return output
}