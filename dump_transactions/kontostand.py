from datetime import datetime, timedelta
from fints.client import FinTS3PinTanClient
import requests

try:
    from config import WANDELWUERZEN, HAUS_KANTE_WURZEN, GEMOEK
except ImportError:
    print('Config error')
    raise


wandelwuerzen_client = FinTS3PinTanClient(
    '43060967',
    WANDELWUERZEN['username'],
    WANDELWUERZEN['password'],
    'https://hbci-pintan.gad.de/cgi-bin/hbciservlet',
    product_id=None,
)

hkw_client = FinTS3PinTanClient(
    '86050200',
    HAUS_KANTE_WURZEN['username'],
    HAUS_KANTE_WURZEN['password'],
    'https://banking-sn5.s-fints-pt-sn.de/fints30',
    product_id=None,
)

gemoek_client = FinTS3PinTanClient(
    '43060967',
    GEMOEK['username'],
    GEMOEK['password'],
    'https://hbci-pintan.gad.de/cgi-bin/hbciservlet',
    product_id=None,
)

def make_report(client):
    account = client.get_sepa_accounts()[0]
    balance = client.get_balance(account)
    transactions = client.get_transactions(account, start_date=datetime.now() - timedelta(days=7))

    messages = []
    messages.append(f':moneybag: Kontostand: *{balance.amount.amount} {balance.amount.currency}*')
    
    if len(transactions) == 0:
        messages.append('_Keine Änderungen in den letzten 7 Tagen_')
        return messages

    messages.append(':money_with_wings: Geldein- und Ausgänge der letzten 7 Tage:')

    for t in transactions:
        d = t.data
        messages.append(
            ':arrows_counterclockwise: {} - {}\n'.format(d['date'], d['applicant_name'])
            + '*{} {}*\n'.format(d['amount'].amount, d['amount'].currency)
            + '{}\n'.format(d['posting_text'])
            + '{}\n'.format(d['purpose'])
        )

    return messages


def send_to_slack(messages, webhook):
    for message in messages:
        requests.post(webhook, json={'text': message, 'username': 'konto', 'icon_emoji': 'money_mouth_face'})

for client, webhook in [
    (wandelwuerzen_client, WANDELWUERZEN['webhook']),
    (hkw_client, HAUS_KANTE_WURZEN['webhook']),
    (gemoek_client, GEMOEK['webhook'])
]:
    try:
        report = make_report(client)
    except:
        send_to_slack(['*Error!*'], webhook)
    else:
        send_to_slack(report, webhook)
