from datetime import datetime, timedelta
import json
from fints.client import FinTS3PinTanClient
import requests
import json
import mt940
import sys

dump_folder='../data'

product_id = "EC449295201FA9BE5040B9154"

try:
    from config import WANDELWUERZEN, HAUS_KANTE_WURZEN
except ImportError:
    print('Config error')
    raise

def dump_account_transactions(client, account_name):
    account = client.get_sepa_accounts()[0]
    iban = account[0]
    now = datetime.now()
    start = now - timedelta(days=90)
    if len(sys.argv) >= 3:
        filename = sys.argv[2]
    else:
        filename = dump_folder + '/' + account_name + '_' + start.strftime('%Y-%m-%d') + '_' + now.strftime('%Y-%m-%d') + '.json'
    transactions = client.get_transactions(account, start_date=start)
    f = open(filename, 'w')
    json.dump(transactions, f, indent=2, cls=mt940.JSONEncoder)
    print("transactions dumped in " + filename)
    f.close()

no_arg = len(sys.argv) < 2
if no_arg or sys.argv[1] == "hkw":
    hkw_client = FinTS3PinTanClient(
        '86050200',
        HAUS_KANTE_WURZEN['username'],
        HAUS_KANTE_WURZEN['password'],
        'https://banking-sn5.s-fints-pt-sn.de/fints30',
        product_id=product_id,
    )
    dump_account_transactions(hkw_client, 'hkw')

elif no_arg or sys.argv[1] == "wandelwuerzen":
    wandelwuerzen_client = FinTS3PinTanClient(
        '43060967',
        WANDELWUERZEN['username'],
        WANDELWUERZEN['password'],
        'https://hbci-pintan.gad.de/cgi-bin/hbciservlet',
        product_id=product_id,
    )
    dump_account_transactions(wandelwuerzen_client, 'wandelwuerzen')
else:
    print("unknown account '%s' provided. only 'hkw' and 'wandelwuerzen' accepted" % (sys.argv[1]))