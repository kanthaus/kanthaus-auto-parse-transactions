- Anleitung: in welchem Ordner?
- prevent packkage-lock.

# Kanthaus Auto Parse Transactions

This repo contains 2 scripts:
- a Python script to a JSON dump of transactions from the bunk for the last 90 days (based on a fork of [kanthaus-finscript](https://gitlab.com/kanthaus/kanthaus-finscript))
- a JS script to parse such a JSON dump, and convert it to CSV lines that can then be opened in LibreOffice and, from there, copy/pasted in the Buchhaltung spreadsheet

Those scripts are bundled into a unique `dump-and-convert.sh` script for convenience.

## Install
1. Install dependencies
    - On Debian-based distributions (like Ubuntu): \
        `sudo apt install git docker.io`
    - On Arch-based distributions: \
        `sudo pacman -S docker git`

1. add user to docker group: \
    `sudo usermod -aG docker $USER`
1. now **restart** your computer
1. clone the repository
    - In the kanthaus finance documentation, there is a special `git clone` command which gives you write access 
    - if you don't need write access, you can also use `git clone https://gitlab.com/kanthaus/kanthaus-auto-parse-transactions.git`
1. go into the newly created directory \
    `cd kanthaus-auto-parse-transactions`


## Configure
1. ensure you are in `kanthaus-auto-parse-transactions`
1. go into folder `dump_transactions` \
    `cd dump_transactions`
1. create file `config.py` \
    `cp config_example.py config.py`
1. open the file in your file browser\
    `kanthaus-auto-parse-transactions` -> `dump_transactions` -> `config.py`
1.  replace the fields like `yourusername` with our bank username and password (stored in the keepass)
1. go back to the main folder \
    `cd ..`


## Run
1. ensure you are in `kanthaus-auto-parse-transactions`
1. Run script
    * HKW: \
    `./dump-and-convert.sh hkw`
    * WaWü: \
    `./dump-and-convert.sh wandelwuerzen`
1. A libreoffice document gets opened. you can now copy the new lines into the existing table.

## Troubleshooting

if you get the error message 
``` Dialog response: 9050 - Die Nachricht enthält Fehler.
Dialog response: 9800 - Dialog abgebrochen
Dialog response: 9340 - Ungültige Auftragsnachricht: Ungültige Signatur.
Traceback (most recent call last):
```
or similar and the script does not run, it is likely that none logged in to the online banking for a while a authorisation with TAN is needed. Just log on to https://www.spk-muldental.de/ with the credentions in KeePass, enter the TAN and after that the script should work again.


## Run individual scripts for development
For example to work on new rules. This requires some undocumented development knowledge and more setup. If you struggle with it, ask one of the devs around :)

### Transactions dumps
Requires:
- python3 ≤ 3.9  (due to [python-fints#134](https://github.com/raphaelm/python-fints/issues/134))
- virtualenv with dependencies from `requirements.in`

```sh
python dump_transactions.py account_name [output_filename]
```

### Convert banking transactions JSON into buchhalting CSV lines
Requires:
- node ≥ 14
- installed dependencies `npm install`

```sh
node generate_csv.js input_filename [account_name] [output_filename]
```
